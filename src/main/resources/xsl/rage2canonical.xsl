<?xml version="1.1" encoding="UTF-8"?>
<!--
  A transformation from FCREPO RAGE Metadata Model xml to canonical RAGE Metadata Model xml (see <http://rageproject.eu/>).
-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:ed="http://greenbytes.de/2002/rfcedit"
    xmlns:exslt="http://exslt.org/common" xmlns:nt="http://www.jcp.org/jcr/nt/1.0"
    xmlns:sv="http://www.jcp.org/jcr/sv/1.0" xmlns:jcr="http://www.jcp.org/jcr/1.0"
    xmlns:rage="http://rageproject.eu/2015/asset/" xmlns:dcterms="http://purl.org/dc/terms/"
    xmlns:adms="http://www.w3.org/ns/adms#" xmlns:dcat="http://www.w3.org/ns/dcat#"
    xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xml="http://www.w3.org/XML/1998/namespace" xmlns:saxon="http://saxon.sf.net/"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:fn="http://www.w3.org/2005/xpath-functions"
    xmlns:xalan="http://xml.apache.org/xalan"
    exclude-result-prefixes="nt ed exslt jcr saxon sv xs math xd xalan fn">

    <xsl:output method="xml" encoding="UTF-8"
        indent="yes" xalan:indent-amount="3" />
    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Sept 15, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> Atanas Georgiev</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="@url[parent::*]">
        <!-- skip the attribute -->
    </xsl:template>
    <xsl:template match="@uuid[parent::*]">
        <!-- skip the attribute -->
    </xsl:template>
    <xsl:template match="@metaIndex[parent::*]">
        <!-- skip the attribute -->
    </xsl:template>    
    <xsl:template match="@name[parent::rage:Asset]">
        <!-- skip the attribute -->
    </xsl:template>        
</xsl:stylesheet>