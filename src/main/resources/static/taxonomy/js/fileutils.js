var FileUtils = (function(){

  var holder = document.getElementById('holder'),
      tests = {
        filereader: typeof FileReader != 'undefined',
        dnd: 'draggable' in document.createElement('span')
      }, 
      support = {
        filereader: document.getElementById('filereader'),
      },
      data = null;

  function readfile(file) {
    if (tests.filereader === true) {
      var reader = new FileReader();
      reader.onload = function (event) {
        data = event.target.result;
      };
      reader.readAsText(file, "UTF-8");
      holder.innerHTML += '<p>Uploaded taxonomy ' + file.name + ' ' + (file.size ? (file.size/1024|0) + 'K' : '');
    }
  }

  function readfiles(files) {
      readfile(files[0]);
  }

  function readAsText() {
    return data;
  }

  if (tests.dnd) { 
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
      this.className = '';
      e.preventDefault();
      readfiles(e.dataTransfer.files);
    }
  }

  return {
    readAsText: readAsText
  }

}());