var rage = (function(opt){

	var config = {
		baseUrl: "https://rage-http-api.localhost:9000",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8"
	};

	function list(opt) {
		var url = config.baseUrl + "/taxonomies",
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", config.accept);
		xhr.send();		
	}

	function get(opt) {
		var url = config.baseUrl + "/taxonomies".concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", config.accept);
		xhr.send();		
	}
	
	function getByURI(opt) {
		var url = config.baseUrl + "/taxonomies".concat("/search", "?themeTaxonomy=", opt.themeTaxonomy),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", config.accept);
		xhr.send();		
	}	

	function post(opt) {
		var url = config.baseUrl + "/taxonomies".concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("POST", url, true);
		xhr.setRequestHeader("Content-Type", config.contentType);
		xhr.send(opt.data);		
	}


	function erase(opt) {
		var url = config.baseUrl + "/taxonomies".concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("DELETE", url, true);
		xhr.setRequestHeader("Accept", config.accept);
		xhr.send();			
	}

	var taxonomy = {
		list: list,		
		get: get,
		getByURI: getByURI,
		post: post,
		erase: erase
	};
	
	var asset = {
		list: list
	};	
	
	return {
		taxonomy: taxonomy,
		asset: asset
	}

}());

