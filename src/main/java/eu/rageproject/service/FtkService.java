/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.service;

import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class FtkService {
	private static final Logger logger = LoggerFactory.getLogger(FtkService.class);
	private String PushNotificationUrl;
	private String schema;
	private String namespaceIdentifier;

	@Autowired
	public FtkService(FtkProperties properties) {
		this.PushNotificationUrl = properties.getPushNotificationUrl();
		this.schema = properties.getSchema();
		this.namespaceIdentifier = properties.getNamespaceIdentifier();
	}

	@JmsListener(destination = "ftk", containerFactory = "myFactory")
	public void receiveMessage(UUID uuid) {

		logger.debug("Received task for asset push notification with uuid {}", uuid);

		JSONObject parameters = new JSONObject();
		JSONObject identifier = new JSONObject();
		identifier.put("identifier", String.format("%s:%s:%s", schema, namespaceIdentifier, uuid));
		parameters.put("parameters", identifier);
		String requestJson = parameters.toString();
		logger.debug(requestJson);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);
		RestTemplate restTemplate = new RestTemplate();
		String responseJson = restTemplate.postForObject(PushNotificationUrl, entity, String.class);
		logger.debug("Ftk Asset Harvesting API Response: {} ", responseJson);
	}
}
