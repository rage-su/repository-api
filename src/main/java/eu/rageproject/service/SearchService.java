/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.service;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import eu.rageproject.model.json.Asset;
import eu.rageproject.model.json.HttpPurlOrgDcTermsTitle;
import eu.rageproject.security.SecurityUtils;

@Service
public class SearchService {

	private String sesameURL;
	private String sesameRepository;
	private String searchQuery;
	private String listQuery;

	@Autowired
	public SearchService(SesameProperties properties) {
		this.sesameURL = properties.getLocation();
		this.sesameRepository = properties.getRepository();
		this.searchQuery = properties.getSearchQuery();
		this.listQuery = properties.getListQuery();
	}

	public ResponseEntity<List<Asset>> list(Integer offset, Integer limit) {
		String query = listQuery;
		Comparator<Asset> sortByTitle = (Asset a1, Asset a2) -> {
			Optional<HttpPurlOrgDcTermsTitle> o1 = a1.getHttpPurlOrgDcTermsTitle().stream().filter(o -> o.getLanguage().equals("en")).findFirst();
			Optional<HttpPurlOrgDcTermsTitle> o2 = a2.getHttpPurlOrgDcTermsTitle().stream().filter(o -> o.getLanguage().equals("en")).findFirst();
			if (!o1.isPresent()) {
				return -1;
			}
			else if (!o2.isPresent()) {
				return 1;
			}
			return o1.get().getValue().compareTo(o2.get().getValue());
		};
				
		return execute(query, offset, limit, sortByTitle);
	}

	public ResponseEntity<List<Asset>> search(String luceneQuery, Integer offset, Integer limit) {
		String query = searchQuery.replace("{LQUERY}", luceneQuery);
		Comparator<Asset> sortByScore = (Asset a1, Asset a2) -> {
			float score1 = a1.getHttpRageprojectEu2015AssetScore().get(0).getValue();
			float score2 = a2.getHttpRageprojectEu2015AssetScore().get(0).getValue();
			return score1 < score2 ? 1 
				     : score1 > score2 ? -1 
				     : 0;
		};		
		return execute(query, offset, limit, sortByScore);
	}

	private ResponseEntity<List<Asset>> execute(String query, Integer offset, Integer limit, Comparator<Asset> sortBy) {
		String userName = SecurityUtils.getCurrentLogin();
		query = query.replace("{PRINCIPAL}", userName);		
		if (offset != null) {
			query = query.replace("{OFFSET}", " OFFSET " + offset).replace("?offset", offset.toString());
		} else {
			query = query.replace("{OFFSET}", "").replace("?offset", "0");
		}
		if (limit != null && limit > 0) {
			query = query.replace("{LIMIT}", " LIMIT " + limit).replace("?limit", limit.toString());
		} else {
			query = query.replace("{LIMIT}", "").replace("?limit", "0");
		}		

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.valueOf("application/sparql-query"));
		headers.setAccept(Arrays.asList(MediaType.valueOf("application/ld+json;charset=utf-8")));

		HttpEntity<String> entity = new HttpEntity<String>(query, headers);
		String url = (new StringBuffer()).append(sesameURL).append("/repositories/").append(sesameRepository)
				.toString();

		ParameterizedTypeReference<List<Asset>> typeRef = new ParameterizedTypeReference<List<Asset>>() {
		};

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters()
        	.add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		ResponseEntity<List<Asset>> response = restTemplate.exchange(url, HttpMethod.POST, entity, typeRef);
		List<Asset> list = response.getBody();
		list.sort(sortBy);
				
		return new ResponseEntity<List<Asset>>(list, response.getStatusCode());		
	}
}
