/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

@Service
public class TransformService {

	@Autowired
	private ResourceLoader resourceLoader;

	private static final String XALAN_TRANSFORMER_FACTORY = "org.apache.xalan.processor.TransformerFactoryImpl";
	public static final String PARAM_ASSET_UUID = "assetUUID";
	public static final String PARAM_ROOT_ELEMENT = "root";
	public static final String PARAM_ARTEFACT_UUID = "artefactUUID";
	public static final String PARAM_ARTEFACT_NAME = "artefactName";
	public static final String PARAM_BASE_URL = "baseURL";
	private final TransformContext transformContext;

	@Autowired
	public TransformService(TransformContext transformContext) {
		this.transformContext = transformContext;
	};

	public String process(String xmlFileName, String xslFileName, Map<String, Object> transformationParameters)
			throws Exception {

		transformationParameters.put(TransformHelper.XSL_TRANSFORM_CONTEXT, transformContext);
		String output = transformRageXmlToJcrXml(xslFileName, xmlFileName, transformationParameters);

		return output;
	}

	public String transformRageXmlToJcrXml(String xslFileName, String xmlFileName,
			Map<String, Object> transformParameters)
			throws TransformerException, ParserConfigurationException, SAXException, IOException {

		Resource resource = resourceLoader.getResource("classpath:xsl/" + xslFileName);
		Document xmlDoc = createDocumentFromXML(xmlFileName);
		Transformer transformer = createSchemaTransformer(resource.getInputStream());

		for (String tpKey : transformParameters.keySet()) {
			transformer.setParameter(tpKey, transformParameters.get(tpKey));
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userID = auth.getName();
		transformer.setParameter("userID", userID);

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		transformer.transform(new DOMSource(xmlDoc), new StreamResult(os));
		return new String(os.toByteArray(), Charset.forName("UTF-8"));
	}

	private Transformer createSchemaTransformer(InputStream xslStream) throws TransformerConfigurationException {
		System.setProperty("javax.xml.transform.TransformerFactory", XALAN_TRANSFORMER_FACTORY);
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		return transformerFactory.newTransformer(new StreamSource(xslStream));
	}

	private Document createDocumentFromXML(String XMLLocation)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		documentBuilderFactory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
		InputStream is = new FileInputStream(XMLLocation);
		return builder.parse(is);
	}

}
