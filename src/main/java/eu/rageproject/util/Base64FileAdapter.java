/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.util;

import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Base64FileAdapter extends XmlAdapter<String, File> {
	@Override
	public String marshal(File file) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Utils.encode64(file, os);
		return new String(os.toByteArray(), "UTF-8");
	}

	@Override
	public File unmarshal(String data) throws Exception {
		return null;
	}
}