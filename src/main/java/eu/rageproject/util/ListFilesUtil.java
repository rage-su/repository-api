/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.core.io.ClassPathResource;

public class ListFilesUtil {
	
	private final List<String> blacklist = Arrays.asList("manifest.xml");
	
    public void listFilesAndFolders(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            System.out.println(file.getName());
        }
    }
    
    public void listFiles(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isFile()){
                System.out.println(file.getName());
            }
        }
    }
    
    public void listFolders(String directoryName){
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList){
            if (file.isDirectory()){
                System.out.println(file.getName());
            }
        }
    }
    
    public void listFilesAndFilesSubDirectories(String baseDirectoryName, String targetDirectoryName){
		
        File target = new File(targetDirectoryName);
        File base = new File(baseDirectoryName);
        File[] fList = target.listFiles();
        for (File file : fList){
            if (file.isFile() && !blacklist.contains(file.getName())){
                System.out.println("listFilesAndFilesSubDirectories: " + base.toURI().relativize(file.toURI()).getPath());
            } else if (file.isDirectory()){
                listFilesAndFilesSubDirectories(baseDirectoryName, file.getAbsolutePath());
            }
        }
    }
    
    
    public static String guessFileType(File file) throws IOException {
        String mimeType = null;

        InputStream mimeTypesStream = (new ClassPathResource("mime.type")).getInputStream(); 
        MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap(mimeTypesStream);
        mimeType = mimeTypesMap.getContentType(file);
        if (mimeType != null)
            return mimeType;

        mimeType = mimeTypesMap.getContentType(file.getName());
        if (mimeType != null)
            return mimeType;

        return mimeType;
    }    
        
}