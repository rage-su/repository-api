/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.endpoint;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import eu.rageproject.exception.InvalidMetadataException;
import eu.rageproject.model.Asset;
import eu.rageproject.repository.FedoraRepository;
import eu.rageproject.service.AssetService;
import eu.rageproject.service.TransformService;
import eu.rageproject.storage.FileStorageService;
import eu.rageproject.storage.StorageFileNotFoundException;
import eu.rageproject.validation.AssetValidator;

@RestController
@CrossOrigin
@RequestMapping("/assets")
public class AssetController {

	@Autowired
	private AssetValidator assetValidator;
	
	@Autowired 
	private JmsTemplate jmsTemplate;	

	private final AssetService assetService;
	private final FileStorageService storageService;
	private final FedoraRepository repository;

	@Autowired
	public AssetController(AssetService assetService, TransformService transformService,
			FileStorageService storageService, FedoraRepository repository) {
		this.assetService = assetService;
		this.storageService = storageService;
		this.repository = repository;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(assetValidator);
	}

	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> create() throws Exception {
		String uuid = assetService.create();
		return new ResponseEntity<String>(linkTo(AssetController.class).slash(uuid).toUri().toString(),
				HttpStatus.CREATED);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) throws Exception {
		String uuid = assetService.upload(file);
		return new ResponseEntity<String>(linkTo(AssetController.class).slash(uuid).toUri().toString(),
				HttpStatus.CREATED);		
	}

	@RequestMapping(value = "/{uuid}/metadata", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public void update(@PathVariable UUID uuid, @Valid @ModelAttribute("metadata") Asset asset,
			BindingResult bindingResult, HttpServletRequest request) throws Exception {
		
		if (bindingResult.hasErrors()) {
			 throw new InvalidMetadataException("Invalid metadata", bindingResult);
		}
		
//		FedoraResource resource = repository.fetchFedoraResource("/" + uuid + "/metadata");
//
//		HttpSession session = request.getSession();
//		Instant fetchedDateTime = Instant.ofEpochMilli((long) session.getAttribute("ResourceFetchedDateTime"));
//
//		Instant modifiedDateTime = resource.getLastModifiedDateTime();
//
//		if (modifiedDateTime.isAfter(fetchedDateTime)) {
//			throw new ContentConflictException("Conflict.");
//		}

		assetService.update(uuid, asset);
        jmsTemplate.convertAndSend("ftk", uuid);		
	}

	@RequestMapping(value = "/{uuid}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable("uuid") UUID uuid) throws Exception {
		assetService.delete(uuid);
		jmsTemplate.convertAndSend("ftk", uuid);
	}

	@RequestMapping(value = "/{uuid}/metadata", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<String> fetchXml(@PathVariable UUID uuid, HttpServletRequest request) throws Exception {
//		FedoraResource resource = repository.fetchFedoraResource("/" + uuid + "/metadata");
//		HttpSession session = request.getSession();
//		session.setAttribute("ResourceFetchedDateTime", resource.getLastModifiedDateTime().toEpochMilli());
		String metadata = assetService.fetchXml(uuid);
		return new ResponseEntity<String>(metadata, HttpStatus.OK);
	}

	@RequestMapping(value = "/{uuid}/copy", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<String> copy(@PathVariable UUID uuid) throws Exception {
		UUID newUUID = UUID.randomUUID();
		assetService.copy(uuid, newUUID);
		URI resourceURI = linkTo(AssetController.class).slash(newUUID).toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(resourceURI);
		return new ResponseEntity<String>(resourceURI.toString(), headers, HttpStatus.CREATED);
	}

	//@see http://www.benashby.com/spring/2013/10/30/serving-large-files-spring-mvc.html
	@RequestMapping(value = "/{uuid}", method = { RequestMethod.GET, RequestMethod.HEAD }, produces = "application/zip")
	@ResponseBody
	public ResponseEntity<InputStreamResource> download(@PathVariable UUID uuid, HttpServletRequest request)
			throws Exception {

		Path resource = storageService.loadAssetPackage(uuid);

		if (HttpMethod.HEAD.matches(request.getMethod())) {
			return ResponseEntity.ok().cacheControl(CacheControl.noStore()).contentLength(resource.toFile().length())
					.contentType(MediaType.parseMediaType("application/zip")).body(null);
		}

		InputStream is = new FileInputStream(resource.toString());
		return ResponseEntity.ok().cacheControl(CacheControl.noStore()).contentLength(resource.toFile().length())
				.contentType(MediaType.parseMediaType("application/zip")).body(new InputStreamResource(is));
	}

	@ExceptionHandler(StorageFileNotFoundException.class)
	public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException e) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(InvalidPathException.class)
	public ResponseEntity<?> handleStorageFileNotFound(InvalidPathException e) {
		e.printStackTrace();
		return ResponseEntity.noContent().build();
	}

}