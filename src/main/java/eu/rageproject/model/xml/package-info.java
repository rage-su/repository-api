//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.11.21 at 09:23:35 AM EET 
//

@javax.xml.bind.annotation.XmlSchema(
		namespace = "http://rageproject.eu/2015/asset/", 
		elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED,
		xmlns = {
				@XmlNs(prefix="rdfs", namespaceURI="http://www.w3.org/2000/01/rdf-schema#"),
				@XmlNs(prefix="xsi", namespaceURI="http://www.w3.org/2001/XMLSchema-instance"),
				@XmlNs(prefix="dcterms", namespaceURI="http://purl.org/dc/terms/"),
				@XmlNs(prefix="dcat", namespaceURI="http://www.w3.org/ns/dcat#"),
				@XmlNs(prefix="foaf", namespaceURI="http://xmlns.com/foaf/0.1/"),
				@XmlNs(prefix="adms", namespaceURI="http://www.w3.org/ns/adms#"),
				@XmlNs(prefix="rdf", namespaceURI="http://www.w3.org/1999/02/22-rdf-syntax-ns#"),
				@XmlNs(prefix="rage", namespaceURI="http://rageproject.eu/2015/asset/")
		})
package eu.rageproject.model.xml;
import javax.xml.bind.annotation.*;
