/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.model.jcr;

import java.io.File;
import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import eu.rageproject.service.AssetService;

@Component
public class NtResourceNodeHelper {
	
	@Autowired
	NtFileNodeHelper ntFileNode;

	private String mimeType;
	private Long hasSize;
	private String sha1;
	private String fileName;
	private String path;
	private String userName;
	private String uuidValue = null;
	private File file;
	
	public NtResourceNodeHelper() {
		
	}
	
	public Node getNode() {
		String createdDate = OffsetDateTime.now().toString();

		Node node = new Node();
		node.setName("jcr:content");

		Property primaryType = new Property();
		primaryType.setName("jcr:primaryType");
		primaryType.setType("Name");
		primaryType.getValue().add("nt:resource");

		Property mixinTypes = new Property();
		mixinTypes.setName("jcr:mixinTypes");
		mixinTypes.setType("Name");
		mixinTypes.setMultiple(true);
		mixinTypes.getValue().add("fedora:Binary");
		
		Property uuid = new Property();
		uuid.setName("jcr:uuid");
		uuid.setType("String");
		uuid.getValue().add(uuidValue);

		Property lastModifiedBy = new Property();
		lastModifiedBy.setName("jcr:lastModifiedBy");
		lastModifiedBy.setType("String");
		lastModifiedBy.getValue().add(userName);

		Property createdBy = new Property();
		createdBy.setName("jcr:createdBy");
		createdBy.setType("String");
		createdBy.getValue().add(userName);

		Property created = new Property();
		created.setName("jcr:created");
		created.setType("Date");
		created.getValue().add(createdDate);

		Property lastModified = new Property();
		lastModified.setName("jcr:lastModified");
		lastModified.setType("Date");
		lastModified.getValue().add(createdDate);

		Property mimeTypeProp = new Property();
		mimeTypeProp.setName("jcr:mimeType");
		mimeTypeProp.setType("String");
		mimeTypeProp.getValue().add(mimeType);		

		Property hasSizeProp = new Property();
		hasSizeProp.setName("jcr:hasSizeProp");
		hasSizeProp.setType("Long");
		hasSizeProp.getValue().add(hasSize.toString());			
		
		Property hasMimeTypeProp = new Property();
		hasMimeTypeProp.setName("ebucore:hasMimeType");
		hasMimeTypeProp.setType("String");
		hasMimeTypeProp.getValue().add(mimeType);			

		Property hasMessageDigestProp = new Property();
		hasMessageDigestProp.setName("premis:hasMessageDigest");
		hasMessageDigestProp.setType("URI");
		hasMessageDigestProp.getValue().add(new StringBuilder().append("urn:sha1:").append(sha1).toString());			
		
		Property fileNameProp = new Property();
		fileNameProp.setName("ebucore:filename");
		fileNameProp.setType("String");
		fileNameProp.getValue().add(fileName);			
		
		Property pathProp = new Property();
		pathProp.setName("rage:path");
		pathProp.setType("String");
		pathProp.getValue().add(path);			

		FileProperty dataProp = new FileProperty();
		dataProp.setName("jcr:data");
		dataProp.setType("Binary");
		dataProp.setValue(file);			
		
		node.getNodeOrProperty().add(primaryType);
		node.getNodeOrProperty().add(mixinTypes);
		node.getNodeOrProperty().add(uuid);
		node.getNodeOrProperty().add(lastModifiedBy);
		node.getNodeOrProperty().add(createdBy);
		node.getNodeOrProperty().add(created);
		node.getNodeOrProperty().add(lastModified);
		node.getNodeOrProperty().add(mimeTypeProp);
		node.getNodeOrProperty().add(hasSizeProp);
		node.getNodeOrProperty().add(hasMimeTypeProp);
		node.getNodeOrProperty().add(hasMessageDigestProp);
		node.getNodeOrProperty().add(fileNameProp);
		node.getNodeOrProperty().add(pathProp);
		node.getNodeOrProperty().add(dataProp);
		
		String ntFileNodeName = AssetService.METADATA_FILE.equals(fileName)?"metadata":null;

		Node n = ntFileNode.getNode(userName, ntFileNodeName);
		n.getNodeOrProperty().add(node);
		return n;
		
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public void setHasSize(Long hasSize) {
		this.hasSize = hasSize;
	}

	public void setSha1(String sha1) {
		this.sha1 = sha1;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setBase64FilePipe(File file) {
		this.file = file;
	}

	public void setUuid(String uuid) {
		this.uuidValue = uuid;
	}
	
}
