/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.model.jcr;

import java.time.OffsetDateTime;
import java.util.UUID;

import org.springframework.stereotype.Component;

@Component
public class NtFileNodeHelper {

	public Node getNode(String userName, String name) {
		String createdDate = OffsetDateTime.now().toString();

		Node node = new Node();
		node.setName((name==null)?UUID.randomUUID().toString():name);

		Property primaryType = new Property();
		primaryType.setName("jcr:primaryType");
		primaryType.setType("Name");
		primaryType.getValue().add("nt:file");

		Property mixinTypes = new Property();
		mixinTypes.setName("jcr:mixinTypes");
		mixinTypes.setType("Name");
		mixinTypes.setMultiple(true);
		mixinTypes.getValue().add("fedora:NonRdfSourceDescription");
		mixinTypes.getValue().add("fedora:Resource");

		Property uuid = new Property();
		uuid.setName("jcr:uuid");
		uuid.setType("String");
		uuid.getValue().add(UUID.randomUUID().toString());

		Property lastModifiedBy = new Property();
		lastModifiedBy.setName("jcr:lastModifiedBy");
		lastModifiedBy.setType("String");
		lastModifiedBy.getValue().add(userName);

		Property createdBy = new Property();
		createdBy.setName("jcr:createdBy");
		createdBy.setType("String");
		createdBy.getValue().add(userName);

		Property created = new Property();
		created.setName("jcr:created");
		created.setType("Date");
		created.getValue().add(createdDate);

		Property lastModified = new Property();
		lastModified.setName("jcr:lastModified");
		lastModified.setType("Date");
		lastModified.getValue().add(createdDate);

		node.getNodeOrProperty().add(primaryType);
		node.getNodeOrProperty().add(mixinTypes);
		node.getNodeOrProperty().add(uuid);
		node.getNodeOrProperty().add(lastModifiedBy);
		node.getNodeOrProperty().add(createdBy);
		node.getNodeOrProperty().add(created);
		node.getNodeOrProperty().add(lastModified);
		
		return node;
	}

}
