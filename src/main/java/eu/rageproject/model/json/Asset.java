/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.rageproject.model.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "@id",
    "http://purl.org/dc/terms/description",
    "http://purl.org/dc/terms/title",
    "http://rageproject.eu/2015/asset/logo",
    "http://rageproject.eu/2015/asset/permissions"
})
public class Asset {

    @JsonProperty("@id")
    private String id;
    @JsonProperty("http://purl.org/dc/terms/description")
    private List<HttpPurlOrgDcTermsDescription> httpPurlOrgDcTermsDescription = new ArrayList<HttpPurlOrgDcTermsDescription>();
    @JsonProperty("http://purl.org/dc/terms/title")
    private List<HttpPurlOrgDcTermsTitle> httpPurlOrgDcTermsTitle = new ArrayList<HttpPurlOrgDcTermsTitle>();
    @JsonProperty("http://rageproject.eu/2015/asset/logo")
    private List<HttpRageprojectEu2015AssetLogo> httpRageprojectEu2015AssetLogo = null;  
    @JsonProperty("http://rageproject.eu/2015/asset/permissions")
    private List<HttpRageprojectEu2015AssetPermission> httpRageprojectEu2015AssetPermissions = new ArrayList<HttpRageprojectEu2015AssetPermission>();
    @JsonProperty("http://rageproject.eu/2015/asset/score")
    private List<HttpRageprojectEu2015AssetScore> httpRageprojectEu2015AssetScore = null;    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("@id")
    public String getId() {
    	String pattern = "^(.*?)([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})(.*?)$";
    	return id.replaceAll(pattern, "$2");    	
    }

    /**
     * 
     * @param id
     *     The @id
     */
    @JsonProperty("@id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The httpPurlOrgDcTermsDescription
     */
    @JsonProperty("http://purl.org/dc/terms/description")
    public List<HttpPurlOrgDcTermsDescription> getHttpPurlOrgDcTermsDescription() {
        return httpPurlOrgDcTermsDescription;
    }

    /**
     * 
     * @param httpPurlOrgDcTermsDescription
     *     The http://purl.org/dc/terms/description
     */
    @JsonProperty("http://purl.org/dc/terms/description")
    public void setHttpPurlOrgDcTermsDescription(List<HttpPurlOrgDcTermsDescription> httpPurlOrgDcTermsDescription) {
        this.httpPurlOrgDcTermsDescription = httpPurlOrgDcTermsDescription;
    }

    /**
     * 
     * @return
     *     The httpPurlOrgDcTermsTitle
     */
    @JsonProperty("http://purl.org/dc/terms/title")
    public List<HttpPurlOrgDcTermsTitle> getHttpPurlOrgDcTermsTitle() {
        return httpPurlOrgDcTermsTitle;
    }

    /**
     * 
     * @param httpPurlOrgDcTermsTitle
     *     The http://purl.org/dc/terms/title
     */
    @JsonProperty("http://purl.org/dc/terms/title")
    public void setHttpPurlOrgDcTermsTitle(List<HttpPurlOrgDcTermsTitle> httpPurlOrgDcTermsTitle) {
        this.httpPurlOrgDcTermsTitle = httpPurlOrgDcTermsTitle;
    }

    /**
     * 
     * @return
     *     The httpRageprojectEu2015AssetLogo
     */    
    @JsonProperty("http://rageproject.eu/2015/asset/logo")
    public List<HttpRageprojectEu2015AssetLogo> getHttpRageprojectEu2015AssetLogo() {
    	return httpRageprojectEu2015AssetLogo;
    }

    /**
     * 
     * @param httpRageprojectEu2015AssetLogo
     *     The http://rageproject.eu/2015/asset/logo
     */    
    @JsonProperty("http://rageproject.eu/2015/asset/logo")
    public void setHttpRageprojectEu2015AssetLogo(List<HttpRageprojectEu2015AssetLogo> httpRageprojectEu2015AssetLogo) {
    	this.httpRageprojectEu2015AssetLogo = httpRageprojectEu2015AssetLogo;
    }    
    
    /**
     * 
     * @return
     *     The httpRageprojectEu2015AssetPermissions
     */
    @JsonProperty("http://rageproject.eu/2015/asset/permissions")
    public List<HttpRageprojectEu2015AssetPermission> getHttpRageprojectEu2015AssetPermissions() {
        return httpRageprojectEu2015AssetPermissions;
    }

    /**
     * 
     * @param httpRageprojectEu2015AssetPermissions
     *     The http://rageproject.eu/2015/asset/permissions
     */
    @JsonProperty("http://rageproject.eu/2015/asset/permissions")
    public void setHttpRageprojectEu2015AssetPermissions(List<HttpRageprojectEu2015AssetPermission> httpRageprojectEu2015AssetPermissions) {
        this.httpRageprojectEu2015AssetPermissions = httpRageprojectEu2015AssetPermissions;
    }

    @JsonProperty("http://rageproject.eu/2015/asset/score")
    public List<HttpRageprojectEu2015AssetScore> getHttpRageprojectEu2015AssetScore() {
    return httpRageprojectEu2015AssetScore;
    }

    @JsonProperty("http://rageproject.eu/2015/asset/score")
    public void setHttpRageprojectEu2015AssetScore(List<HttpRageprojectEu2015AssetScore> httpRageprojectEu2015AssetScore) {
    this.httpRageprojectEu2015AssetScore = httpRageprojectEu2015AssetScore;
    }    
    
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
