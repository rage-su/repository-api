/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.storage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import eu.rageproject.service.AssetService;

@Service
public class FileSystemStorageService implements FileStorageService {

	private final Path rootLocation;
	private final long maxSize;

	@Autowired
	public FileSystemStorageService(FileStorageProperties properties) {
		this.rootLocation = Paths.get(properties.getLocation());
		this.maxSize = properties.getMaxSize();
	}

	public Path storeAssetPackage(MultipartFile file, String uuid) {
		try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
			}
			if (file.getOriginalFilename() == null || file.getOriginalFilename().isEmpty()) {
				throw new StorageException("Rejecting empty file name.");
			}						
			if (file.getSize() > this.maxSize) {
				throw new StorageException(
						"File " + file.getOriginalFilename() + " exceeds size limit " + this.maxSize);
			}
			final String lowerCaseFileName = file.getOriginalFilename().toLowerCase();
			if (!lowerCaseFileName.endsWith("zip")) {
				throw new StorageException("Allowed extensions only zip");
			}			
			Files.copy(file.getInputStream(), this.rootLocation.resolve(uuid + ".zip"));
			
			return this.rootLocation.resolve(uuid + ".zip");
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
		}
	}

	public File writeToTempFile(String content) {
		try {
			File file = File.createTempFile(UUID.randomUUID().toString(), ".tmp");

			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(content);
			bw.close();

			return file;
		} catch (IOException e) {
			throw new StorageException("Faild to write to temporary file.");
		}
	}

	public File createTempFile() {
		try {
			File file = File.createTempFile(UUID.randomUUID().toString(), ".tmp");
			return file;
		} catch (IOException e) {
			throw new StorageException("Faild to create temporary file.");
		}
	}	
	
	@Override
	public void copy(String sourceFileName, String destFileName) {
		try {
			Path file = load(sourceFileName);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				Files.copy(file, this.rootLocation.resolve(destFileName));
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + sourceFileName);
			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + sourceFileName, e);
		} catch (IOException e) {
			throw new StorageException("Failed to copy file: " + sourceFileName);
		}
	}

	public Stream<Path> loadAll() {
		try {
			return Files.walk(this.rootLocation, 1).filter(path -> !path.equals(this.rootLocation))
					.map(path -> this.rootLocation.relativize(path));
		} catch (IOException e) {
			throw new StorageException("Failed to read stored files", e);
		}

	}

	public Path loadAssetPackage(UUID uuid) {
		return load(uuid + ".zip");
	}	
	
	public Path loadAssetPackageMetadata(String uuid) {
		return load(uuid + File.separator + AssetService.METADATA_FILE);
	}
	
	public Path load(String filename) {
		Path resource = rootLocation.resolve(filename);
		if (!resource.toFile().exists()) {
			throw new StorageFileNotFoundException("File not found.");
		}
		return resource;
	}

	public Resource loadAsResource(String filename) throws StorageFileNotFoundException {
		try {
			Path file = load(filename);
			Resource resource = new UrlResource(file.toUri());
			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new StorageFileNotFoundException("Could not read file: " + filename);

			}
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}
	
	public void deleteAssetPackageWorkingDir(String uuid) {
		FileSystemUtils.deleteRecursively(rootLocation.resolve(uuid).toFile());
	}	

	public void deleteAll() {
		FileSystemUtils.deleteRecursively(rootLocation.toFile());
	}

	public void delete(String filename) {
		try {
			Files.delete(this.rootLocation.resolve(filename));
		} catch (IOException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filename, e);
		}
	}

	public void init() {
		try {
			Files.createDirectory(rootLocation);
		} catch (IOException e) {
			throw new StorageException("Could not initialize storage", e);
		}
	}

	@Override
	public void store(MultipartFile file) {
		// TODO Auto-generated method stub
		
	}

}