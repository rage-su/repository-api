/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.validation;

import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import eu.rageproject.model.Asset;

@Component
public class AssetValidator implements Validator {

	@Autowired
	private ResourceLoader resourceLoader;

	@Override
	public boolean supports(Class<?> clazz) {
		return Asset.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Asset asset = (Asset) target;

		try {
			StreamSource xmlFile = new StreamSource(new StringReader(asset.getMetadata()));
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			Source schemaFile = new StreamSource(
					resourceLoader.getResource("classpath:static/xsd/DefaultProfile.xsd").getFile());
			Schema schema = schemaFactory.newSchema(schemaFile);
			javax.xml.validation.Validator validator = schema.newValidator();

			validator.setErrorHandler(new ErrorHandler() {
				@Override
				public void warning(SAXParseException exception) throws SAXException {
					errors.rejectValue("metadata", "metadata.invalid", exception.getLocalizedMessage());
				}

				@Override
				public void fatalError(SAXParseException exception) throws SAXException {
					errors.rejectValue("metadata", "metadata.invalid", exception.getLocalizedMessage());
				}

				@Override
				public void error(SAXParseException exception) throws SAXException {
					errors.rejectValue("metadata", "metadata.invalid", exception.getLocalizedMessage());
				}
			});

			validator.validate(xmlFile);

		} catch (SAXException e) {
			errors.rejectValue("metadata", "metadata.invalid", e.getLocalizedMessage());
		} catch (IOException e) {
			errors.rejectValue("metadata", "metadata.invalid", e.getLocalizedMessage());
		}
	}
}