/**
 * Copyright (C) 2016-2017 Sofia University (https://www.uni-sofia.bg/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.rageproject.config;

public enum ArtefactSection {
	usage("", "rage:hasUsage"), implementation("/solution", "rage:hasImplementation"), design("/solution",
			"rage:hasDesign"), requirements("/solution", "rage:hasRequirements"), tests("/solution", "rage:hasTests");

	private String parent;

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getPredicat() {
		return predicat;
	}

	public void setPredicat(String predicat) {
		this.predicat = predicat;
	}

	private String predicat;

	ArtefactSection(String parent, String predicat) {
		this.parent = parent;
		this.predicat = predicat;
	}
}